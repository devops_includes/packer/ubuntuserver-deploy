variable "proxmox_url" {
  type = string
}
variable "node" {
  type = string
}
variable "token_id" {
  type = string
}
variable "token_secret" {
  type = string
}

variable "pool" {
  type = string
}

variable "vm_name" {
  type = string
}

variable "iso_file" {
  type = string
  default = "iso-storage:iso/ubuntu-23.04-live-server-amd64.iso"
}

variable "insecure_skip_tls_verify" {
  type = bool
  default = true
}

variable "memory" {
  type = number
  default = 8192
}

variable "cores" {
  type = number
  default = 6
}

variable "scsi_controller" { 
  type = string
  default = "virtio-scsi-single"
}

variable "disk_size" {
  type = string
  default = "30G"
}

variable "storage_pool" {
  type = string
  default = "local-zfs"
}

variable "type" {
  type = string
  default = "scsi"
}

variable "bridge" {
  type = string
  default = "vmbr99"
}

variable "model" { 
  type = string
  default = "virtio"
}

variable "cloud_init" { 
  type = bool
  default = true
}

variable "cloud_init_storage_pool" {
  type = string
  default = "local-zfs"
} 

variable "boot_command" {
  type = list(string)
  default = [
    "c<wait>",
    "linux /casper/vmlinuz --- autoinstall ds=\"nocloud-net;seedfrom=http://10.10.99.37/auto/\"",
    "<enter><wait5>",
    "initrd /casper/initrd",
    "<enter><wait>",
    "boot",
    "<enter>"
  ]
}

variable "boot_wait" { 
  type = string
  default= "10s"
}

variable "unmount_iso" {
  type = bool
  default = true
}

variable "qemu_agent" {
  type = bool
  default = true
}

variable "ssh_username" {
  type = string
}

 variable "ssh_port" {
  type = string
}
 
variable "ssh_timeout" {
  type = string
  default = "25m"
}

variable "ssh_private_key" {
  type = string
}
